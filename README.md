# ProjektSukulent

Find this page on: https://mirna-krpic.gitlab.io/projektsukulent/


## Description
This page is made as a project for the Basics of Web and Mobile Application Development course.
On this page you can find information and images of my small collection of succulent plants. 
There is a more detailed page for Aloe plant only.

